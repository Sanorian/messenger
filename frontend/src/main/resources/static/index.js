if (!localStorage.getItem("id")){
    location.href = `${window.location.href}login`;
}

const webSocket = new WebSocket(`ws://${window.location.href.slice(0, -1)}:8080/sendmessage`);

webSocket.onopen = function(e) {};

webSocket.onmessage = function(event) {
    document.getElementById("messages").innerHTML += ``;
    alert(`[message] Данные получены с сервера: ${event.data}`);
};

webSocket.onerror = function(error) {
    alert(`[error]`);
};

window.addEventListener("beforeunload", function (e) {
    webSocket.close();
});

function sendMessage(){
    const id = localStorage.getItem("id");
    const password = localStorage.getItem("password");
    const message = document.getElementsByTagName("textarea")[0].value;
    webSocket.send(JSON.stringify({
        id: id,
        password: password,
        message: message
    }));
}
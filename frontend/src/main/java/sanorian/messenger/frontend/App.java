package com.notenet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.text.html.HTMLDocument;
import java.io.IOException;

@SpringBootApplication
@Controller
public class NoteNetApplication {
	static String index, registration, login;

	public static void main(String[] args) throws IOException {
		index = com.notenet.FileReader.readFile("pages/index.html");
		registration = com.notenet.FileReader.readFile("pages/registration.html");
		login = com.notenet.FileReader.readFile("pages/login.html");
		SpringApplication.run(NoteNetApplication.class, args);
	}

	@GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String index() {
		return index;
	}

	@GetMapping(value = "/registration", produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String registration() {
		return registration;
	}
	
	@GetMapping(value = "/login", produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String login() {
		return login;
	}
}

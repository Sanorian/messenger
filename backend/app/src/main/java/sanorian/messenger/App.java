package java.sanorian.messenger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.text.html.HTMLDocument;
import java.io.IOException;

@SpringBootApplication
@Controller
public class App {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(NoteNetApplication.class, args);
	}

	@GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String index() {

	}

	@PostMapping(value = "/registration", produces = MediaType.TEXT_HTML_VALUE)
	public String registration(@RequestBody NewUser user) {

	}

	@PostMapping(value = "/login", produces = MediaType.TEXT_HTML_VALUE)
	public String login(@ResponseBody LoginningUser user) {

	}
}
